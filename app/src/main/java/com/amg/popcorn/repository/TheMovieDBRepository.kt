package com.amg.popcorn.repository

import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.amg.popcorn.common.MyApp
import com.amg.popcorn.network.TheMovieDBClient
import com.amg.popcorn.network.TheMovieDBService
import com.amg.popcorn.network.model.Movie
import com.amg.popcorn.network.model.PopularMoviesResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TheMovieDBRepository {

    var theMovieDBService: TheMovieDBService? = null
    var theMovieDBClient: TheMovieDBClient? = null
    var popularMovies: MutableLiveData<List<Movie>>? = null

    init {
        theMovieDBClient = TheMovieDBClient.instance
        theMovieDBService = theMovieDBClient?.getTheMovieDBService()

        popularMovies = popularMovies()

    }

    fun popularMovies(): MutableLiveData<List<Movie>> {
        if (popularMovies == null) {
            popularMovies = MutableLiveData<List<Movie>>()
        }

        val call: Call<PopularMoviesResponse>? = theMovieDBService?.getPopularMovies()

        call?.enqueue(object : Callback<PopularMoviesResponse>{
            override fun onFailure(call: Call<PopularMoviesResponse>, t: Throwable) {
                Toast.makeText(MyApp.instance, "Error en la llamada", Toast.LENGTH_SHORT).show()
            }

            override fun onResponse(call: Call<PopularMoviesResponse>, response: Response<PopularMoviesResponse>) {
                if (response.isSuccessful) {
                    popularMovies?.value = response.body()?.results
                }
            }

        })
        return popularMovies as MutableLiveData<List<Movie>>
    }

}