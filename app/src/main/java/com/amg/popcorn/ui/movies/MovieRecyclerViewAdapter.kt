package com.amg.popcorn.ui.movies

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import coil.load
import coil.transform.CircleCropTransformation
import com.amg.popcorn.R
import com.amg.popcorn.common.Constantes
import com.amg.popcorn.network.model.Movie

class MovieRecyclerViewAdapter(var popularMovies: List<Movie>) : RecyclerView.Adapter<MovieRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_movie_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = popularMovies[position]
        holder.tv_movie.text = item.original_title
        holder.tv_rating.text = item.vote_average.toString()
        holder.iv_movie.load(Constantes.IMAGE_BASE_URL + item.poster_path) {
            crossfade(true)
            //placeholder(R.drawable.ic_cine)
            transformations(CircleCropTransformation())
        }
    }

    override fun getItemCount(): Int = popularMovies.size

    fun setData(popularMovies: List<Movie>) {
        this.popularMovies = popularMovies;
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val iv_movie: ImageView = view.findViewById(R.id.iv_movie)
        val tv_movie: TextView = view.findViewById(R.id.tv_movie)
        val tv_rating: TextView = view.findViewById(R.id.tv_rating)

    }
}