package com.amg.popcorn.ui.movies

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.amg.popcorn.network.model.Movie
import com.amg.popcorn.repository.TheMovieDBRepository

class MovieViewModel: ViewModel() {

    private var theMovieDBRepository: TheMovieDBRepository
    private var popularMovies: LiveData<List<Movie>>


    init {

        theMovieDBRepository = TheMovieDBRepository()
        popularMovies = theMovieDBRepository.popularMovies()

    }

    fun getPopularMovies() : LiveData<List<Movie>> {
        return popularMovies
    }


}