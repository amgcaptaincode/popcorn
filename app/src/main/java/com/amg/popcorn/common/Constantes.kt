package com.amg.popcorn.common

class Constantes {

    companion object {

        public val BASE_URL = "https://api.themoviedb.org/3/"

        public val API_KEY = "ff1541ffb94b89e3dc599b860dec920d"
        public val IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w500"

        public val URL_PARAM_API_KEY = "api_key"
        public val URL_PARAM_LANGUAJE = "language"

    }

}