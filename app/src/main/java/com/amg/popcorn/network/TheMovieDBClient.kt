package com.amg.popcorn.network

import com.amg.popcorn.common.Constantes
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class TheMovieDBClient {

    private val theMovieDBService: TheMovieDBService
    private val retrofit: Retrofit

    companion object {

        var instance: TheMovieDBClient? = null
                get() {
                    if (field == null) {
                        field = TheMovieDBClient()
                    }
                    return field
                }

    }

    // Constructor
    init {

        // Incluir el interceptor que hemos definido
        val okHtttpBuilder = OkHttpClient.Builder();
        okHtttpBuilder.addInterceptor(TheMovieDBInterceptor())

        val interceptorBody = HttpLoggingInterceptor()
        interceptorBody.level = HttpLoggingInterceptor.Level.BODY

        okHtttpBuilder.addInterceptor(interceptorBody)

        val client = okHtttpBuilder.build();

        // Construir el cliente de Retrofit
        retrofit = Retrofit.Builder()
            .baseUrl(Constantes.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()

        // Instanciamos el servicio de Retrofit a partir del objeto Retrofit
        theMovieDBService = retrofit.create(TheMovieDBService::class.java)

    }

    fun getTheMovieDBService() = theMovieDBService

}