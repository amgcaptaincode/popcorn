package com.amg.popcorn.network

import com.amg.popcorn.network.model.PopularMoviesResponse
import retrofit2.Call
import retrofit2.http.GET

interface TheMovieDBService {

    @GET("movie/popular")
    fun getPopularMovies() : Call<PopularMoviesResponse>

}